import Vue from 'vue'
import Router from 'vue-router'
import index from '@/components/index'
import cat from '@/components/cat'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: index
    },
    {
        path: '/cat',
        name: 'cat',
        component: cat,
        children:[
          {
            path: '/tyg/cat1',
            name: 'tyg_cat1',
            component: resolve => require(['../components/tyg/cat1.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/tyg/cat2',
            name: 'tyg_cat2',
            component: resolve => require(['../components/tyg/cat2.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/tyg/cat3',
            name: 'tyg_cat3',
            component: resolve => require(['../components/tyg/cat3.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/27dormitory/brief',
            name: 'dormitory27brief',
            component: resolve => require(['../components/27dormitory/brief.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/27dormitory/cat1',
            name: 'dormitory27cat1',
            component: resolve => require(['../components/27dormitory/cat1.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/27dormitory/cat2',
            name: 'dormitory27cat2',
            component: resolve => require(['../components/27dormitory/cat2.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/27dormitory/cat3',
            name: 'dormitory27cat3',
            component: resolve => require(['../components/27dormitory/cat3.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/27dormitory/cat4',
            name: 'dormitory27cat4',
            component: resolve => require(['../components/27dormitory/cat4.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/elusive/cat1',
            name: 'elusive_cat1',
            component: resolve => require(['../components/elusive/cat1.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/elusive/cat2',
            name: 'elusive_cat2',
            component: resolve => require(['../components/elusive/cat2.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/elusive/cat3',
            name: 'elusive_cat3',
            component: resolve => require(['../components/elusive/cat3.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/unknown/cat1',
            name: 'unknown_cat1',
            component: resolve => require(['../components/unknown/cat1.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/unknown/cat2',
            name: 'unknown_cat2',
            component: resolve => require(['../components/unknown/cat2.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/gossip/about',
            name: 'gossip_about',
            component: resolve => require(['../components/gossip/about.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/gossip/story1',
            name: 'gossip_story1',
            component: resolve => require(['../components/gossip/story1.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/about/about',
            name: 'about_about',
            component: resolve => require(['../components/about/about.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
          {
            path: '/about/updata',
            name: 'about_updata',
            component: resolve => require(['../components/about/updata.vue'], resolve),
            meta: {
              keepAlive: false
            }
          },
        ]
      },
  ]
})
