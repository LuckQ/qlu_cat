import Vue from 'vue'
import App from './App.vue'
import router from './router'
import canvas from '@/components/useless/canvas'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
Vue.use(VueAwesomeSwiper)


Vue.config.productionTip = false;
Vue.component('canvas',canvas);

(function(w) {
  function IsPC() {
    var userAgentInfo = navigator.userAgent;
    var Agents = ["Android", "iPhone",
       "SymbianOS", "Windows Phone",
       "iPad", "iPod"];
    var flag = true;
    for (var v = 0; v < Agents.length; v++) {
       if (userAgentInfo.indexOf(Agents[v]) > 0) {
          flag = false;
          break;
       }
    }
    return flag;
 }
 Vue.prototype.PCflag = IsPC()
  if(!IsPC()){
    var _doc = w.document
    var _docEle = _doc.documentElement
    var _docWidth = _docEle.getBoundingClientRect().width
    var _scale = _docWidth / 750
    _docEle.style.fontSize = (_scale * 100) + 'px'
    Vue.prototype.relativeRate = _scale
    w.onload = function() {

      document.documentElement.style.fontSize = (_docWidth / 750 * 70) + 'px'
      
    }
  }else {
    document.documentElement.style.fontSize = '60px'
  }
  
})(window)
// require('./css/reset.css');
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
